# -*- coding: utf-8 -*-

import pytest

from app import structures


@pytest.mark.parametrize('constructor', [
    structures.ClassPoint,
    structures.DataClassPoint,
    structures.NamedtupleClassPoint,
    structures.NamedtuplePoint,
    structures.ClassSlotsPoint,
])
def test_structures(benchmark, constructor):
    result = benchmark.pedantic(structures.point_factory,
                                args=(constructor, 5,),
                                iterations=100,
                                rounds=20_000)
    assert True
