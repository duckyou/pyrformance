# -*- coding: utf-8 -*-

import pytest

from app import string_formating


@pytest.mark.parametrize('function', [
    string_formating.concatenation,
    string_formating.old_style,
    string_formating.format_method,
    string_formating.format_string,
    string_formating.using_join,
])
def test_string_formating(benchmark, function):
    result = benchmark.pedantic(function, iterations=100, rounds=200_000)
    assert 'word1, word2, word3' == result
