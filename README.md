# Python. Fast and furious

## Environment and Hardware

- Ubuntu 18.04
- Python 3.7 (CPython)
- poetry
- pytest
- pytest-benchmarks

- Intel(R) Core(TM) i5-8400 CPU @ 2.80GHz (6 cores)
- ATA Samsung SSD 860 (scsi)
- RAM Kingston DDR4 16GB (2666 MT/s) x2

## Table of Contents

1. [x] Introduction
    1. [x] Big-O - Short in
    2. [x] Big-O cheatsheet for Python structures
2. [x] Rules of Optimization
3. [ ] Profiling and Benchmarking
4. [ ] Optimization tips
5. [ ] Optimization tools
6. [ ] Conclusion

## 1. Introduction

### 1.1 Big-O - Short in

![Big-O Complexity Chart](resources/img/big_o.jpeg)

Big-O Notation is a statistical measure, used to describe the complexity of the
algorithm.


#### Complexity examples:

```python
# Constant complexity O(1)
def constant_algorithm(items):
    return items[0] + items[0]

# Linear complexity O(n) and O(2n)
def linear_algorithm(items):
    out = 0
    for item in items:
        out += item
    return out

def linear_algorithm_2(items):
    return linear_algorithm(items) + linear_algorithm(items)

# Quadratic complexity O(n^2)
def quadratic_algorithm(items):
    out = 0
    for item in items:
        for item2 in items:
            out += item + item2
    return out
```

#### Finding complexity of the algorithm:

```python
def complex_algorithm(items):                   # N^2 + 3N + 2 -> N^2
    items_len = len(items)                      # 1
    new_items = list()                          # 1
    for i in range(items_len):                  # N
        new_items.append(i)                     # N
    return quadratic_algorithm(new_items) \     # N^2
        + linear_algorithm_2(items)             # N
```

#### Worst vs Best Case Complexity

Usually, when someone asks you about the complexity of the algorithm he is
asking you about the worst case complexity.

```python
# items = [1, 2, 3, 4, 5]
# Best case     -> num=1 -> O(1)
# Worst case    -> num=6 -> O(N)
def search_algorithm(num, items):   # N + 3 -> N
    for item in items:              # N
        if item == num:             # 1
            return True             # 1
    return False                    # 1
```

In addition to best and worst case complexity, you can also calculate the
average complexity of an algorithm.

### 1.2 Big-O cheatsheet for Python structures

https://wiki.python.org/moin/TimeComplexity

## 2. Rules of Optimization

> **Premature optimization is the root of all evil.**
>
> \- Donald Knuth

### **Rule №1: Don't do it!**

**Make it run**: We have to get the software in a working state, and ensure
that it produces the correct results. This exploratory phase serves to better
understand the application and to spot major design issues in the early stages.

### **Rule №2: Don't do it, yet.**

**Make it right**: We want to ensure that the design of the program is solid.
Refactoring should be done before attempting any performance optimization.
This really helps separate the application into independent and cohesive units
that are easier to maintain.

### **Rule №3: Profile before optimizing!**

**Make it fast**: Once our program is working and is well structured, we can
focus on performance optimization. We may also want to optimize memory usage
<u>if that constitutes an issue.</u>


## 3. Profiling and Benchmarking

**Profiling** is the technique that allows us to pinpoint the most
resource-intensive spots in an application. A profiler is a program that runs
an application and monitors how long each function takes to execute, thus
detecting the functions in which your application spends most of its time.

**Benchmarks** are small scripts used to assess the total execution time of
your application.

#### Custom decorator (`app/utils/__init__.py`)

```python
# -*- coding: utf-8 -*-
import time

def timeit(func):
    """Simple and small benchmark decorator."""
    def wrapper(*args, **kwargs):
        time_before = time.time()
        result = func(*args, **kwargs)
        exec_time = round(time.time() - time_before, 2)
        print(f'{func.__name__} executed for {exec_time}s')
        return result
    return wrapper
```

```python
>>> from app.base import just_a_function
>>> just_a_function(sleep=0.999)
()
{'sleep': 0.99}
just_a_function executed for 0.99s
```

#### Linux `time` command

```sh
time python main.py
()
{'sleep': 0.99}
just_a_function executed for 0.99s

real    0m1.010s
user    0m0.007s
sys     0m0.012s
```

- **real**: The actual time spent running the process from start to finish,
as if it was measured by a human with a stopwatch
- **user**: The cumulative time spent by all the CPUs during the computation
- **sys**: The cumulative time spent by all the CPUs during system-related
tasks, such as memory allocation

#### `timeit` module

This module runs a snippet of code in a loop for n times and measures the total
execution times.
Then, it repeats the same operation r times (by default,the value of r is 3)
and records the time of the best run.
Due to this timing scheme, timeit is an appropriate tool to accurately time
small statements in isolation.

```sh
python -m timeit -s 'from app.base import just_a_function' \
    'just_a_function(sleep=0.99)'
()
{'sleep': 0.99}
just_a_function executed for 0.99s
()
{'sleep': 0.99}
just_a_function executed for 0.99s
()
{'sleep': 0.99}
just_a_function executed for 0.99s
()
{'sleep': 0.99}
just_a_function executed for 0.99s
()
{'sleep': 0.99}
just_a_function executed for 0.99s
()
{'sleep': 0.99}
just_a_function executed for 0.99s
1 loop, best of 5: 991 msec per loop
```

#### `pytest-benchmark` (`tests.test_base.py`)

ref: https://pytest-benchmark.readthedocs.io/en/stable/

```python
# -*- coding: utf-8 -*-

import pytest

from app.base import just_a_function


def test_just_a_function(benchmark):
    # benchmark something
    result = benchmark(just_a_function)
```

```sh
----------------------------------------------- benchmark: 1 tests -----------------------------------------------
Name (time in ms)            Min      Max     Mean  StdDev   Median     IQR  Outliers      OPS  Rounds  Iterations
------------------------------------------------------------------------------------------------------------------
test_just_a_function     10.1469  10.3222  10.2663  0.0373  10.2763  0.0206     17;14  97.4059      98           1
------------------------------------------------------------------------------------------------------------------
```

#### `profile` and `cProfile` modules

Two profiling modules are available through the Python standard library:
- The `profile` module: This module is written in pure Python and adds a
significant overhead to the program execution. Its presence in the standard
library is because of its vast platform support and because it is easier to extend.
- The `cProfile` module: This is the main profiling module, with an interface
equivalent to profile. It is written in C, has a small overhead, and is suitable as
a general purpose profiler.

```sh
python -m cProfile main.py
()
{'sleep': 0.99}
just_a_function executed for 0.99s
         560 function calls (549 primitive calls) in 0.991 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        ...
        1    0.000    0.000    0.000    0.000 __init__.py:1(<module>)
        1    0.000    0.000    0.000    0.000 __init__.py:2(<module>)
        1    0.000    0.000    0.000    0.000 __init__.py:4(timeit)
        1    0.000    0.000    0.991    0.991 __init__.py:6(wrapper)
        1    0.000    0.000    0.000    0.000 base.py:1(<module>)
        1    0.000    0.000    0.991    0.991 base.py:6(just_a_function)
        1    0.000    0.000    0.991    0.991 main.py:2(<module>)
        3    0.000    0.000    0.000    0.000 {built-in method _imp._fix_co_filename}
       15    0.000    0.000    0.000    0.000 {built-in method _imp.acquire_lock}
        1    0.000    0.000    0.000    0.000 {built-in method _imp.is_builtin}
        3    0.000    0.000    0.000    0.000 {built-in method _imp.is_frozen}
       15    0.000    0.000    0.000    0.000 {built-in method _imp.release_lock}
        6    0.000    0.000    0.000    0.000 {built-in method _thread.allocate_lock}
        6    0.000    0.000    0.000    0.000 {built-in method _thread.get_ident}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.__import__}
        3    0.000    0.000    0.000    0.000 {built-in method builtins.any}
      4/1    0.000    0.000    0.991    0.991 {built-in method builtins.exec}
       18    0.000    0.000    0.000    0.000 {built-in method builtins.getattr}
       12    0.000    0.000    0.000    0.000 {built-in method builtins.hasattr}
        7    0.000    0.000    0.000    0.000 {built-in method builtins.isinstance}
        9    0.000    0.000    0.000    0.000 {built-in method builtins.len}
        3    0.000    0.000    0.000    0.000 {built-in method builtins.print}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.round}
        2    0.000    0.000    0.000    0.000 {built-in method builtins.setattr}
        9    0.000    0.000    0.000    0.000 {built-in method from_bytes}
        3    0.000    0.000    0.000    0.000 {built-in method marshal.loads}
        9    0.000    0.000    0.000    0.000 {built-in method posix.fspath}
        1    0.000    0.000    0.000    0.000 {built-in method posix.getcwd}
        1    0.000    0.000    0.000    0.000 {built-in method posix.listdir}
       16    0.000    0.000    0.000    0.000 {built-in method posix.stat}
        1    0.991    0.991    0.991    0.991 {built-in method time.sleep}
        2    0.000    0.000    0.000    0.000 {built-in method time.time}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
        3    0.000    0.000    0.000    0.000 {method 'endswith' of 'str' objects}
        3    0.000    0.000    0.000    0.000 {method 'extend' of 'list' objects}
        7    0.000    0.000    0.000    0.000 {method 'get' of 'dict' objects}
       26    0.000    0.000    0.000    0.000 {method 'join' of 'str' objects}
        3    0.000    0.000    0.000    0.000 {method 'read' of '_io.FileIO' objects}
       21    0.000    0.000    0.000    0.000 {method 'rpartition' of 'str' objects}
       46    0.000    0.000    0.000    0.000 {method 'rstrip' of 'str' objects}
        2    0.000    0.000    0.000    0.000 {method 'startswith' of 'str' objects}
```

#### RunSnakeRun

#### SnakeViz

## 4. Optimization Levels

### Design optimization

Change DB type, change programming language :) and etc.

### Algorithms and Data Structures optimization

| type | size (bytes) | creating time | search in |
| ---- | ------------ | ------------- | --------- |
| tuple | 48 | 8.29 ns ± 0.00721 | 60.9 ns ± 0.0778 |
| list | 64 | 36.9 ns ± 0.0335 | 61.4 ns ± 0.242 |
| set | 224 | 98.7 ns ± 0.16 ns | 23.6 ns ± 0.204 |
| dict | 240 | 116 ns ± 0.591 | 23.4 ns ± 0.178 |

```python
def factorial_one(n):
    out = 1
    for i in range(1, n+1):
        out = out * i
    return out

def factorial_two(n):
    if n == 0:
        return 1
    return n * factorial_two(n - 1)

assert factorial_one(0) == factorial_two(0)
```

```python
%timeit factorial_one(950)

137 µs ± 205 ns per loop (mean ± std. dev. of 7 runs, 10000 loops each)
```

```python
%timeit factorial_two(950)

204 µs ± 725 ns per loop (mean ± std. dev. of 7 runs, 1000 loops each)
```

#### Use math 0_o

Sum all numbers from 0 to N

```python
N = 10_000

s = sum(range(1, N + 1))    # 1.14 ms ± 9.07 µs per loop

s = int(N * (1 + N) / 2)    # 149 ns ± 0.141 ns per loop
```

### Source code optimization <-

### And:

https://hackernoon.com/which-is-the-fastest-version-of-python-2ae7c61a6b2b

- Build optimization
- Compile optimization
- Runtime optimization

CPython, PyPy, JPython etc. (Maybe Numba)

### Optimization is not just about speed...

## Optimization tips


### Filter list

```python


```

## Ref's:

| Type | Title | Link |
| ---- | ----- | ---- |
| Text | Which is the fastest version of Python? | https://hackernoon.com/which-is-the-fastest-version-of-python-2ae7c61a6b2b |
| Book | Gorelick M., Ozsvald I. - High Performance Python | http://shop.oreilly.com/product/0636920028963.do |
| Book | Lanaro G. - Python High Performance | https://www.amazon.com/Python-Performance-Programming-Gabriele-Lanaro/dp/1783288450 |
| Text | TimeComplexity | https://wiki.python.org/moin/TimeComplexity |
| Text | Rules Of Optimization | http://wiki.c2.com/?RulesOfOptimization |
| Text | Big O Notation and Algorithm Analysis with Python Examples | https://stackabuse.com/big-o-notation-and-algorithm-analysis-with-python-examples/ |
| Text | Big-O: How Code Slows as Data Grows | https://nedbatchelder.com/text/bigo.html |
| Video | Ned Batchelder - Big-O: How Code Slows as Data Grows - PyCon 2018 | https://www.youtube.com/watch?v=duvZ-2UK0fc |
| Video | Sebastian Witowski - Writing faster Python | https://www.youtube.com/watch?v=YjHsOrOOSuI |

### Bonuses:

| Type | Title | Link |
| ---- | ----- | ---- |
| Video | Nina Zakharenko - Memory Management in Python - The Basics - PyCon 2016 | https://www.youtube.com/watch?v=F6u5rhUQ6dU |
| Video | Jake VanderPlas - Performance Python: Seven Strategies for Optimizing Your <u>Numerical Code</u> | https://www.youtube.com/watch?v=zQeYx87mfyw |
