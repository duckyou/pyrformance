

```python
# imports
import sys
```

#### Type Sizes

1. tuple
2. list
3. set
4. dict


```python
sys.getsizeof(tuple())
```




    48




```python
sys.getsizeof(list())
```




    64




```python
sys.getsizeof(set())
```




    224




```python
sys.getsizeof(dict())
```




    240



#### Creating

1. tuple
1. list
1. set
1. dict


```python
%timeit t = (1, 2, 3, 4, 5, 6)
```

    8.29 ns ± 0.00721 ns per loop (mean ± std. dev. of 7 runs, 100000000 loops each)



```python
%timeit l = [1, 2, 3, 4, 5, 6]
```

    36.9 ns ± 0.0335 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)



```python
%timeit s = {1, 2, 3, 4, 5, 6}
```

    98.7 ns ± 0.16 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)



```python
%timeit d = {'1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6}
```

    116 ns ± 0.591 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)


#### Search in (`.index()`)

1. Dict & Set
2. List & Tuple


```python
d = {'1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6}
%timeit '6' in d
```

    23.4 ns ± 0.178 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)



```python
s = {1, 2, 3, 4, 5, 6}
%timeit 6 in s
```

    23.6 ns ± 0.204 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)



```python
l = [1, 2, 3, 4, 5, 6]
%timeit 6 in l
```

    61.4 ns ± 0.242 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)



```python
t = (1, 2, 3, 4, 5, 6)
%timeit 6 in t
```

    60.9 ns ± 0.0778 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)


#### ----------------------------------


```python
def meh(value):
    if value in [2, 4, 8, 16, 32]:
        return True
    return False

def heh(value, __in={2, 4, 8, 16, 32}): return value in __in

def hmm(value):
    return value in [2, 4, 8, 16, 32]

def hum(value):
    return value in {2, 4, 8, 16, 32}
```


```python
%timeit meh(33)
```

    98.5 ns ± 0.0746 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)



```python
%timeit heh(33)
```

    62.3 ns ± 0.0735 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)



```python
%timeit hmm(33)
```

    95.6 ns ± 0.467 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)



```python
%timeit hum(33)
```

    57.2 ns ± 0.448 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)


### -------------------------------------


```python
def factorial_one(n):
    out = 1
    for i in range(1, n+1):
        out = out * i
    return out

def factorial_two(n):
    if n == 0:
        return 1
    return n * factorial_two(n - 1)

assert factorial_one(0) == factorial_two(0)
```


```python
%timeit factorial_one(950)
```

    137 µs ± 205 ns per loop (mean ± std. dev. of 7 runs, 10000 loops each)



```python
%timeit factorial_two(950)
```

    204 µs ± 725 ns per loop (mean ± std. dev. of 7 runs, 1000 loops each)



```python
N = 100_000
```


```python
%timeit sum(range(1, N + 1))
sum(range(1, N + 1))
```

    1.14 ms ± 9.07 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)





    5000050000




```python
%timeit int(N * (1 + N) / 2)
int(N * (1 + N) / 2)
```

    149 ns ± 0.141 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)





    5000050000




```python
def my_filter_1(elements):
    out = []
    for element in elements:
        if element % 2:
            out.append(element)
    return out

def my_filter_2(elements):
    return list(filter(lambda x: x % 2, elements))

def my_filter_3(elements):
    return [element for element in elements if element % 2]

ELEMENTS = list(range(100_000))
```


```python
%timeit my_filter_1(ELEMENTS)
```

    3.83 ms ± 17.3 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)



```python
%timeit my_filter_2(ELEMENTS)
```

    6.64 ms ± 64.1 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)



```python
%timeit my_filter_3(ELEMENTS)
```

    2.68 ms ± 5.82 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)



```python
class Foo:
    bar = 'baz'

foo = Foo()
```


```python
%%timeit
if hasattr(foo, 'hello') and hasattr(foo, 'bar') and hasattr(foo, 'baz'):
    foo.hello
```

    47.7 ns ± 0.211 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)



```python
%%timeit 
try:
    foo.hello
    foo.bar
    foo.baz
except AttributeError:
    pass
```

    308 ns ± 1.52 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)



```python
%timeit 99_999 in ELEMENTS
```

    807 µs ± 5.48 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)



```python
SELEMENTS = set(ELEMENTS)
%timeit 99_999 in SELEMENTS
%timeit SELEMENTS = set(ELEMENTS)
```

    35.8 ns ± 0.123 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)
    1.25 ms ± 4.57 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)



```python
%timeit sorted(ELEMENTS)
%timeit ELEMENTS.sort()
```

    514 µs ± 2.56 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)
    280 µs ± 1.15 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)



```python
def square(number):
    return number**2

%timeit [square(i) for i in range(10_000)]
```

    2.31 ms ± 20.4 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)



```python
%timeit [i**2 for i in range(10_000)]
```

    1.85 ms ± 1.42 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)



```python
%timeit []
%timeit list()
```

    15.1 ns ± 0.194 ns per loop (mean ± std. dev. of 7 runs, 100000000 loops each)
    54.1 ns ± 1.05 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)



```python
%timeit {}
%timeit dict()
```

    24.4 ns ± 0.0266 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)
    62.1 ns ± 1.08 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)
















































```python

```
