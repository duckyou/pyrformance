# -*- coding: utf-8 -*-


def concatenation():
    return 'word1' + ', ' + 'word2' + ', ' + 'word3'


def old_style():
    return '%s, %s, %s' % ('word1', 'word2', 'word3')


def format_method():
    return '{}, {}, {}'.format('word1', 'word2', 'word3')


def format_string():
    return f"{'word1'}, {'word2'}, {'word3'}"


def using_join():
    return ', '.join(['word1', 'word2', 'word3'])
