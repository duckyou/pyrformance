# -*- coding: utf-8 -*-
from typing import NamedTuple
from collections import namedtuple
from dataclasses import dataclass


class ClassPoint:
    def __init__(self, x, y):
        self.x = x
        self.y = y


@dataclass
class DataClassPoint:
    x: float
    y: float


class NamedtupleClassPoint(NamedTuple):
    x: float
    y: float


NamedtuplePoint = namedtuple('NamedtuplePoint', 'x, y')


class ClassSlotsPoint:
    __slots__ = ('x', 'y',)

    def __init__(self, x, y):
        self.x = x
        self.y = y


def point_factory(constructor, count):
    return tuple(
        constructor(i, i+i)
        for i in range(count)
    )
