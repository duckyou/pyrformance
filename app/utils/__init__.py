# -*- coding: utf-8 -*-
import time

def timeit(func):
    """Simple and small benchmark decorator."""
    def wrapper(*args, **kwargs):
        time_before = time.time()
        result = func(*args, **kwargs)
        exec_time = round(time.time() - time_before, 2)
        print(f'{func.__name__} executed for {exec_time}s')
        return result
    return wrapper
