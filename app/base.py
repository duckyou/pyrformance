import time

from app.utils import timeit


@timeit
def just_a_function(*args, **kwargs):
    """Just a function for pytest-bechmark testing."""
    sleep = kwargs.get('sleep', 0.01)
    print(args)
    print(kwargs)
    time.sleep(sleep)
